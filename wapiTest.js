import WPAPI from "wpapi";

const wp = new WPAPI({
    endpoint:'http://haxlotuyo.com/wp-json',
});

async function fetchPost() {
    try {
        const posts = await wp.posts().get();
        return posts;
    } catch(e) {
        console.log(e);
        return[];
    }
}