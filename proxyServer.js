var httpProxy = require('http-proxy');
let fs = require('fs');

httpProxy.createServer({
    target: {
        host: 'localhost',
        port: 3000
    },
    ssl: {
        key: fs.readFileSync('./.cert/key.pem','utf-8'),
        cert: fs.readFileSync('./.cert/cert.pem', 'utf8')
    }
}).listen(3000);